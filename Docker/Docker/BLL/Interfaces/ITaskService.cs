﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Common.Models.DTO;

namespace ProjectStructure.BL.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetAllTasks();

        void CreateTask(TaskDTO task);
        void UpdateTask(TaskDTO task);
        void DeleteTask(int taskId);
    }
}
